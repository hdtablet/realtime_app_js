/* global document, window, feathers, moment, io */

// Establish a Socket.io connection
const socket = io();
// Initialize our Feathers client application through Socket.io
// with hooks and authentication.
const client = feathers();

client.configure(feathers.socketio(socket));
// Use localStorage to store our login token
client.configure(feathers.authentication({
  storage: window.localStorage
}));

Vue.config.devtools = true;

var controlNotification = new Vue({
  el: '#controlNotification',
  data: {
       messagesNotifications: [] 
  }, 
  created : async function() {
    await client.authenticate();
  },
  methods: {
    pushData : function (data) {
        this.messagesNotifications.push(data)
    },
    findNotifications : async function () {

      const messages2 = await client.service('notifications').find({
        query: {
          $sort: {createdAt: -1},
          $limit: 25
        }
      });
      this.messagesNotifications =  messages2.data;

    }
  },
  mounted: function () {
      this.$nextTick(  async function () {

            const credentials = {
              email:"test",
              password: "test"
            };

            const payload = Object.assign({ strategy: 'local' }, credentials);
            await client.authenticate(payload);
    

             this.findNotifications()
             client.service('notifications').on('created', this.pushData);          
      })
  }
  
});
